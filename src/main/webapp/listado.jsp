<%-- 
    Document   : listado
    Created on : 23-10-2021, 15:46:10
    Author     : axeld
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.evfinal.entity.Palabra"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabra> lista = (List<Palabra>) request.getAttribute("lista");

    Iterator<Palabra> itlista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Listado de consultas</h1>
        <form  name="form" action="PalabraController" method="GET">

            <table class="table" style="text-align:center" width="50%" border="1" >
                <thead>
                    <tr>
                        <th>Palabra consultada:  </th>
                        <th>Fecha consulta:   </th>
                    </tr>
                </thead>
                <tbody>
                    <%while (itlista.hasNext()) {
                    Palabra pal = itlista.next();%>

                    <tr>
                        <td><%=pal.getPalabra() %></td> 
                        <td><%=pal.getFecha()%></td> 

                        
                    </tr>
                    <%}%>  


                </tbody>


            </table>
        </form>
    </body>
        <footer id="footer">
            <p>Axel Donoso Sección 51</p>
            <p>Link Bitbucket: https://adonosos@bitbucket.org/adonosos/examenaxeldonoso.git</p>
        </footer>
</html>
