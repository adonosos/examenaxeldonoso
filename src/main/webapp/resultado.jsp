<%-- 
   Document   : resultado
   Created on : 21-10-2021, 15:19:40
   Author     : axeld
--%>

<%@page import="root.evfinal.dto.RespuestaDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    RespuestaDTO pal = (RespuestaDTO) request.getAttribute("respuestaDTO");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Resultado:</h2>
            <br>
        <p>Palabra consultada:    <%= pal.getPalabra()%></p>
        <p>Sisgnificado:   <%= pal.getSignificado()%></p>
        <p>Fuente: Oxford Dictionaries</p>
        <br><br><br>
        
        
    </body>
    <footer id="footer">
            <p>Axel Donoso Sección 51</p>
            <p>Link Bitbucket: https://adonosos@bitbucket.org/adonosos/examenaxeldonoso.git</p>
        </footer>
</html>