/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evfinal.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import root.evfinal.dao.PalabraJpaController;
import root.evfinal.dto.RespuestaDTO;
import root.evfinal.entity.Palabra;
import root.evfinal.rest.PalabraRest;

/**
 *
 * @author axeld
 */
@WebServlet(name = "PalabraController", urlPatterns = {"/PalabraController"})
public class PalabraController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PalabraController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
   PalabraJpaController dao = new PalabraJpaController();
         String accion=request.getParameter("accion");
         String palabra=request.getParameter("palabra");
         DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
         
         String fecha = dtf.format(LocalDateTime.now());
          if (accion.equals("listado de consultas")) {
             List<Palabra> listaPalabras=dao.findPalabraEntities();
         
        request.setAttribute("lista", listaPalabras);
        
       request.getRequestDispatcher("listado.jsp").forward(request, response);
        }
          
          
          
          
          
         
       Client client = ClientBuilder.newClient();
          Palabra pal = new Palabra();
  

     WebTarget myResource = client.target("https://examenaxeldonoso.herokuapp.com/api/significado/"+palabra);
     RespuestaDTO respuestaDTO=  myResource.request(MediaType.APPLICATION_JSON).get(RespuestaDTO.class);
    
     pal.setPalabra(palabra);
     pal.setFecha(fecha);
        try {
            dao.create(pal);
        } catch (Exception ex) {
            Logger.getLogger(PalabraController.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     
     
     request.setAttribute("respuestaDTO", respuestaDTO);
     request.getRequestDispatcher("resultado.jsp").forward(request, response);

      
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
