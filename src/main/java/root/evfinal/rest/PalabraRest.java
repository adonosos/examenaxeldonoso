/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evfinal.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evfinal.dto.PalabraDTO;
import root.evfinal.dto.RespuestaDTO;
import root.evfinal.entity.Palabra;

/**
 *
 * @author axeld
 */
@Path("significado")
public class PalabraRest {
    
 @GET
 @Path("/{palabra}")
 @Produces(MediaType.APPLICATION_JSON)
 public Response consultaSignificado(@PathParam("palabra") String palabra){
 
         
        RespuestaDTO dto = new RespuestaDTO();
        dto.setPalabra(palabra);
        
        
        Client client = ClientBuilder.newClient();
        
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+palabra);
        
        PalabraDTO pal=  myResource.request(MediaType.APPLICATION_JSON).header("app_id","650b87d2").header("app_key","556f3c2f80aa6c235a36887f51bb5ab9").get(PalabraDTO.class);
       
        String definicion = pal.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
        dto.setSignificado(definicion);
        return Response.ok(200).entity(dto).build();
 }
    
}
