/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evfinal.dto;

import java.util.ArrayList;

/**
 *
 * @author axeld
 */
public class RespuestaDTO {
    
    private String palabra;
  private String significado;

    /**
     * @return the palabra
     */
    public String getPalabra() {
        return palabra;
    }

    /**
     * @param palabra the palabra to set
     */
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    /**
     * @return the significado
     */
    public String getSignificado() {
        return significado;
    }

    /**
     * @param significado the significado to set
     */
    public void setSignificado(String significado) {
        this.significado = significado;
    }
}
    