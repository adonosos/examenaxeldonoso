/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evfinal.dto;

import java.util.List;

/**
 *
 * @author Ripley
 */
public class LexicalEntries {

    /**
     * @return the derivatives
     */
    public List<Derivative> getDerivatives() {
        return derivatives;
    }

    /**
     * @param derivatives the derivatives to set
     */
    public void setDerivatives(List<Derivative> derivatives) {
        this.derivatives = derivatives;
    }

    /**
     * @return the entries
     */
    public List<Entry> getEntries() {
        return entries;
    }

    /**
     * @param entries the entries to set
     */
    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
  private List<Compounds> compounds;
    
    private List<Derivative> derivatives;
    private List<Entry> entries;  
      private String language;
       private LexicalCategory lexicalCategory;
        private String text;
    /**
     * @return the compounds
     */
    public List<Compounds> getCompounds() {
        return compounds;
    }

    /**
     * @param compounds the compounds to set
     */
    public void setCompounds(List<Compounds> compounds) {
        this.compounds = compounds;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the lexicalCategory
     */
    public LexicalCategory getLexicalCategory() {
        return lexicalCategory;
    }

    /**
     * @param lexicalCategory the lexicalCategory to set
     */
    public void setLexicalCategory(LexicalCategory lexicalCategory) {
        this.lexicalCategory = lexicalCategory;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
   
}

